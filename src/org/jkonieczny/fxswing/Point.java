package org.jkonieczny.fxswing;

/**
 * Created by jkonieczny on 10.01.15.
 */
public class Point {
    public double x, y;
    public Point(){

    }
    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }
}
