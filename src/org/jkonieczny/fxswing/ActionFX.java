package org.jkonieczny.fxswing;

import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCodeCombination;

/**
 * JavaFX AbstractAction prosthesis
 */
public abstract class ActionFX implements EventHandler<ActionEvent> {
    protected Property<Boolean> enabledProperty;
    protected KeyCodeCombination keyCode;
    public String name;
    protected String text;

    protected ActionFX(String name, String text){
        this.text = text;
        this.name = name;
    }

    private void bindEnabled(Property<Boolean> property) {
        if (enabledProperty != null){
            property.bind(enabledProperty);
        } else {
            enabledProperty = property;
        }
    }

    public void setEnabled(boolean enabled) {
        if(enabledProperty == null) {
            enabledProperty = new SimpleBooleanProperty();
        }
        enabledProperty.setValue(enabled);
    }

    public void setOn(Button button){
        button.setOnAction(this);
        bindEnabled(button.disableProperty());
    }
    public void setOn(MenuItem menuItem){
        menuItem.setOnAction(this);
        bindEnabled(menuItem.disableProperty());
    }

    public MenuItem getAsMenuItem() {
        MenuItem item = new MenuItem(text);
        if (keyCode != null)
            item.setAccelerator(keyCode);
        setOn(item);
        return item;
    }
}
