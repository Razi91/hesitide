package org.jkonieczny.hesit.afs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by jkonieczny on 30.09.14.
 */
public abstract class AFile {
    public abstract String getPath();
    public abstract String getFilename();
    public abstract ByteArrayInputStream getInputStream();
    public abstract ByteArrayOutputStream getOutputStream();
    public abstract void remove();

    public abstract void save();
    public abstract void load();

}
