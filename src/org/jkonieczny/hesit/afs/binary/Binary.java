package org.jkonieczny.hesit.afs.binary;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;
import org.jkonieczny.hesit.afs.AFS;
import org.jkonieczny.hesit.afs.AFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class Binary extends AFS {

    Kryo kryo;
    List<BFile> files;
    File file;

    public Binary(String path) {
        this.file = new File(path);
        files = new ArrayList<>(32);
        kryo = new Kryo();
        kryo.register(ArrayList.class, new CollectionSerializer());
        if (file.exists()) {
            //load();
        }
    }

    @Override
    public AFile createFile(String path, String filename) throws FileAlreadyExistsException {
        if (getFile(path, filename) != null) {
            System.out.println("Plik istnieje");
            throw new FileAlreadyExistsException(path + "/" + filename);
        }
        BFile file = new BFile(this, path, filename);
        files.add(file);
        return file;
    }

    @Override
    public List<? extends AFile> getFiles() {
        return files;
    }

    @Override
    public AFile getFile(String path, String name) {
        int deep = 0;
        for (BFile file : files) {
            if (file.getPath().equals(path) && file.getFilename().equals(name))
                return file;
        }
        return null;
    }

    @Override
    public void save() {
        System.out.println("save " + file.getAbsolutePath());
        try {
            Output out = new Output(new FileOutputStream(file));
            kryo.writeObject(out, files);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAs(File file) {
        this.file = file;
        System.out.println("save " + file.getAbsolutePath());
        try {
            Output out = new Output(new FileOutputStream(file));
            kryo.writeObject(out, files);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load() {
        System.out.println("load " + file.getAbsolutePath());
        try {
            Input in = new Input(new FileInputStream(file));
            files = kryo.readObject(in, ArrayList.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void remove(AFile file) {
        files.remove(file);
    }
}
