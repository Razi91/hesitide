package org.jkonieczny.hesit.afs.binary;

import org.jkonieczny.hesit.afs.AFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by jkonieczny on 30.09.14.
 */

public class BFile extends AFile implements Serializable {
    private String path;
    private String filename;
    private transient Binary fs;

    byte content[];

    private transient ByteArrayInputStream input;
    private transient ByteArrayOutputStream output;

    private BFile(){

    }

    public BFile(Binary fs, String path, String filename) {
        this.fs = fs;
        this.path = path;
        this.filename = filename;
        load();
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public ByteArrayInputStream getInputStream() {
        if (input == null) input = new ByteArrayInputStream(content);
        return input;
    }

    @Override
    public ByteArrayOutputStream getOutputStream(){
        if (output != null)
            try {
                output.close();
            } catch (IOException e) {
            }
        output = new OutputStream(this);
        return output;
    }

    @Override
    public void remove() {
        fs.remove(this);
    }

    @Override
    public void save() {
        //fs.save();
        System.out.println("save "+filename);
    }

    @Override
    public void load() {
        //fs.load();
        System.out.println("load "+filename);
    }

    class OutputStream extends ByteArrayOutputStream {
        BFile file;
        public OutputStream(BFile file){
            this.file = file;
        }
        @Override
        public void close() throws IOException {
            file.content = this.toByteArray();
            file.save();
            super.close();
        }
    }
}
