package org.jkonieczny.hesit.afs;

import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.util.List;

/**
 * Created by jkonieczny on 30.09.14.
 */
public abstract class AFS {
    /**
     * Create new file in this file system
     * @param path base path
     * @param filename file name
     * @return empty file
     * @throws FileAlreadyExistsException when file already exists
     */
    public abstract AFile createFile(String path, String filename) throws FileAlreadyExistsException;

    /**
     * Get list of all files
     * @return list of files
     */
    public abstract List<? extends AFile> getFiles();

    /**
     * returns file
     * @param path path of file
     * @param name file name
     * @return
     */
    public abstract AFile getFile(String path, String name);

    /**
     * Performs saving whole filesystem
     */
    public abstract void save();
    /**
     * Chooses new path and performs saving whole filesystem
     */
    public abstract void saveAs(File file);

    /**
     * Loads or reloads whole filesystem clearing caches if there are any
     */
    public abstract void load();
}
