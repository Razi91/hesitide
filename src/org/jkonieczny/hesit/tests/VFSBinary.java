package org.jkonieczny.hesit.tests;

import org.jkonieczny.hesit.afs.AFile;
import org.jkonieczny.hesit.afs.binary.Binary;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class VFSBinary {
    public static void main(String args[]) {
        String path = "/tmp/test.hes";
        Binary vfs = new Binary(path);
        System.out.println("wczytano: " + vfs.getFiles().size());
        AFile file1 = null;
        AFile file2 = null;
        try {
            file1 = vfs.createFile("", "f1.p");
            file2 = vfs.createFile("", "f2.p");
        } catch (FileAlreadyExistsException e) {
            e.printStackTrace();
            return;
        }

        ByteArrayOutputStream out1 = file1.getOutputStream();
        ByteArrayOutputStream out2 = file2.getOutputStream();

        try {
            System.out.println("zapisuj");
            out1.write("test1_plik pierwszy 12".getBytes());
            out2.write("test2_plik drugi 21".getBytes());
            out1.close();
            out2.close();
            vfs.save();
            System.out.println("zapisano: " + vfs.getFiles().size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        vfs = new Binary(path);
        vfs.load();
        System.out.println("wczytano: " + vfs.getFiles().size());
        for(AFile file: vfs.getFiles()){
            System.out.println("contains file: [" + file.getPath() + "]" + file.getFilename());
        }
        file1 = vfs.getFile("", "f1.p");
        file2 = vfs.getFile("", "f2.p");

        ByteArrayInputStream in1 = file1.getInputStream();
        ByteArrayInputStream in2 = file2.getInputStream();

        {
            int n = in1.available();
            byte[] bytes = new byte[n];
            in1.read(bytes, 0, n);
            String s = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(file1.getFilename());
            System.out.println(s);
        }
        {
            int n = in2.available();
            byte[] bytes = new byte[n];
            in2.read(bytes, 0, n);
            String s = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(file2.getFilename());
            System.out.println(s);
        }
    }
}
