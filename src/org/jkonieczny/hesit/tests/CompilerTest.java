package org.jkonieczny.hesit.tests;

import org.jkonieczny.hesit.compilers.JavaScript;
import org.jkonieczny.hesit.language.*;
import org.jkonieczny.hesit.language.instructions.Condition;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jkonieczny on 06.10.14.
 */
public class CompilerTest {

    private static void explore(String intent, ArrayList<Code> codes){
        for (Code c:codes){
            if(c instanceof Instructions) {
                System.out.println(intent + c.getId());
            }
            else if(c instanceof If) {
                System.out.println(intent + "if " + c.getId() + ": " + ((If) c).getOnTrue().size());
                explore(intent + "|  ", ((If) c).getOnTrue());
                System.out.println(intent + "if not " + c.getId() + ": " + ((If) c).getOnFalse().size());
                explore(intent + "|  ", ((If) c).getOnFalse());
            }
            else if(c instanceof GoTo) {
                System.out.println(intent + "goto " + c.getId());
            }
        }
    }

    private static void testA(){
        Code a = new Instructions(1);
        Code b = new Instructions(2);
        Code c = new Instructions(3);
        If d = new If(4, new Condition());
        If dp = new If(40, new Condition());
        If e = new If(5, new Condition());
        Code f = new Instructions(6);

        GoTo gd = new GoTo(4);
        GoTo gdp = new GoTo(40);
        GoTo ge = new GoTo(5);

        d.addOn(true, b, c, gd);
        d.addOn(false, b, e);

        dp.addOn(true, b, c, gdp);
        dp.addOn(false, ge);

        e.addOn(true, c, dp);
        e.addOn(false, f);

        Code codes[] = {a,b,c,d};
        ArrayList<Code> codes2 = Tools.precompile(new ArrayList<>(Arrays.asList(codes)));
        explore("", codes2);
    }

    public static void testB(){
        Code a = new Instructions(1);
        If b = new If(2, new Condition());
        Code c = new Instructions(3);
        If d = new If(4, new Condition());
        Code e = new Instructions(5);
        Code f = new Instructions(6);

        GoTo gb = new GoTo(2);

        b.addOn(true, c, d);
        b.addOn(false, f);

        d.addOn(true, gb);
        d.addOn(false, e, gb);

        Code codes[] = {a, b};
        ArrayList<Code> codes2 = Tools.precompile(new ArrayList<>(Arrays.asList(codes)));
        explore("", codes2);
        Function fun = new Function("testB");
        fun.addCodes(codes2);

        JavaScript compiler = new JavaScript();
        compiler.compile(fun);
        System.out.println(compiler.getCode());
    }

    public static void main(String arg[]) {
        testB();
    }
}