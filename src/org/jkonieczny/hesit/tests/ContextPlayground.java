package org.jkonieczny.hesit.tests;

import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.*;
import java.util.List;

/**
 * Created by jkonieczny on 06.10.14.
 */
public class ContextPlayground {
    public static class Obj {
        public int val;
        public Obj(){val=10;}
        public void inc(){val++;}
        public void foo(){
            System.out.println("asd");
        }
        @Override
        public String toString(){
            return "Obj "+val;
        }
    }
    public static void main(String arg[]) {
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine jython = factory.getEngineByName("jython");
        //NashornScriptEngineFactory engineManager = new NashornScriptEngineFactory();
        ScriptEngine global = factory.getEngineByName("nashorn");
        ScriptEngine local = factory.getEngineByName("nashorn");
        try {
            Obj o = new Obj();
            global.setBindings(global.getBindings(ScriptContext.ENGINE_SCOPE), ScriptContext.GLOBAL_SCOPE);
            local.setBindings(global.getBindings(ScriptContext.GLOBAL_SCOPE), ScriptContext.GLOBAL_SCOPE);
            jython.setBindings(global.getBindings(ScriptContext.GLOBAL_SCOPE), ScriptContext.GLOBAL_SCOPE);

            global.put("obj", o);
            Double foo = new Double(5);
            global.put("foo", foo);
            global.put("foo2", new Integer(2));
            global.eval("foo = foo/foo2");
            global.eval("var bar = function (a) {return a*2}");
            jython.eval("def x2(a):" +
                    "  return 2*a");
            global.eval("obj.val = 5");
            jython.eval("obj.val = int(bar.call('bar', [6]))");
            global.put("x2", jython.get("x2"));
            //global.eval("obj.val = x2(2)");
            System.out.println("obj.val = " + ((Obj) global.get("obj")).val);
            System.out.println("obj.val = " + global.get("bar").getClass().getName());
            System.out.println("obj.val = " + jython.get("x2").getClass().getName());
            jython.eval("obj.foo()");
            System.out.println("foo = " + foo);

            for(String key: global.getBindings(ScriptContext.ENGINE_SCOPE).keySet()){
                System.out.println(global.get(key).getClass().getName()+" "+key + " = "+global.get(key).toString());
            }
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }
}
/*
global foo: 1
local foo: 1
global bar: null
local bar: 2
=========================
global foo: 1
local foo: 4
global bar: 3
local bar: 2
 */