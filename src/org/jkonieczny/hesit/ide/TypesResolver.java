package org.jkonieczny.hesit.ide;

import org.jkonieczny.hesit.ide.paradigms.Paradigm;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jkonieczny on 28.12.14.
 */
public class TypesResolver {
    public abstract class Type {
        public abstract boolean isParadigm();
    }

    public abstract class ParadigmType extends Type {

        @Override
        public boolean isParadigm() {
            return true;
        }

        public abstract Class<? extends Paradigm> getParadigmClass();
    }

    static Map<String, Type> types = new HashMap<>();

    public static void addType(String name, Type type) {
        types.put(name, type);
    }

    public static Type getType(String name) {
        if (types.containsKey(name)) {
            return types.get(name);
        }
        return null;
    }
}
