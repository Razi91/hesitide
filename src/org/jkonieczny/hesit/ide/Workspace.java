package org.jkonieczny.hesit.ide;

import org.jkonieczny.hesit.ide.gui.fx.MainWindowFx;
import org.jkonieczny.hesit.ide.project.Project;
import org.jkonieczny.hesit.ide.runtime.Runner;

/**
 * Created by jkonieczny on 05.10.14.
 */
public class Workspace {
    static Workspace instance;
    public static Workspace getWorkspace() {
        //if(instance == null) instance = new Workspace();
        return instance;
    }

    private Project project;
    private MainWindowFx window;

    public static void init(MainWindowFx window){
        instance = new Workspace();

    }

    public final Project getProject(){
        return project;
    }
    public final MainWindowFx getWindow() {
        return window;
    }


    public void setProject(Project project){
        this.project = project;
        Runner.getRunner().reset();
        //TODO
    }

    public void setWindow(MainWindowFx window){
        this.window = window;
        //TODO
    }



}
