package org.jkonieczny.hesit.ide.core;

import org.jkonieczny.hesit.ide.core.interfaces.SelectionListener;

import java.util.ArrayList;

/**
 * Created by jkonieczny on 03.10.14.
 */
public class Select {
    ArrayList<SelectionListener> listeners;
    static Select instance;
    static {
        instance = new Select();
    }
    public Select(){

    }

    public static Select getSelection(){
        return instance;
    }

    public void fire(){
        for (SelectionListener listener: listeners) {

        }
    }
}
