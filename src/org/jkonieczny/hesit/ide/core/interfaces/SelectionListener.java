package org.jkonieczny.hesit.ide.core.interfaces;

/**
 * Created by jkonieczny on 03.10.14.
 */
public abstract class SelectionListener<T> {
    public abstract void selected(Selection<T> selection);

}
