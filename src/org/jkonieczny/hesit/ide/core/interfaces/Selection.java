package org.jkonieczny.hesit.ide.core.interfaces;

import org.jkonieczny.hesit.ide.core.Select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jkonieczny on 03.10.14.
 */
public class Selection<T> {
    List<T> items;

    public Selection(){

    }
    public Selection(T... items) {
        this.items = new ArrayList<T>(Arrays.asList(items));
    }

    public void select(T... o) {
        items.add((T) o);
    }

    public Class[] getSelectedTypes() {
        return null;
    }

    public void fire(){
        Select.getSelection().fire();
    }
}
