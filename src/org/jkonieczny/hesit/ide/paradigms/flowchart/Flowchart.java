package org.jkonieczny.hesit.ide.paradigms.flowchart;

import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import org.jkonieczny.fxswing.Point;
import org.jkonieczny.hesit.afs.AFile;
import org.jkonieczny.hesit.ide.paradigms.Paradigm;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.Block;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.StartBlock;
import org.jkonieczny.hesit.ide.paradigms.flowchart.fx.EditorFx;

import javax.swing.*;
import java.io.ByteArrayInputStream;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class Flowchart extends Paradigm {

    ObservableList<Block> blocks = FXCollections.observableArrayList();

    EditorFx editor;

    public ObservableList<Block> getBlocks() {
        return blocks;
    }

    public static Paradigm create(AFile file) {
        Flowchart f = new Flowchart(file);
        return f;
    }

    AFile file;

    public Flowchart(AFile file){
        this.file = file;
        StartBlock start = new StartBlock(this);
        blocks.add(start);
        ((EditorFx)getEditorFx()).addBlock(start, new Point(100.0, 100.0));
    }

    @Override
    public void save(){
    }

    @Override
    public void load() {

    }

    @Override
    public Node getEditorFx() {
        if (editor == null)
            this.editor = new EditorFx(this);
        return editor;
    }
    public void closeEditor(){
        editor = null;
    }

    public static String getFileExtension(){
        return "flow";
    }
}
