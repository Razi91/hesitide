package org.jkonieczny.hesit.ide.paradigms.flowchart;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import org.jkonieczny.fxswing.Point;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.Block;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.BlockListener;
import org.jkonieczny.hesit.ide.paradigms.flowchart.fx.EditorFx;

/**
 * Created by jkonieczny on 08.01.15.
 */
public class Arrow implements BlockListener {

    protected Block source;
    protected Block destination;
    protected CubicCurve curve = new CubicCurve();
    protected Circle arrow = new Circle();

    protected EditorFx editor;

    public Arrow(Block source, Block destination) {
        this.source = source;
        this.destination = destination;
        geometryChanged(null);
        curve.setStroke(Color.BLACK);
        curve.setStrokeWidth(1);
        curve.setStrokeLineCap(StrokeLineCap.BUTT);
        curve.setFill(Color.TRANSPARENT);
        arrow.setRadius(5);
        arrow.setFill(Color.BLACK);
        source.addListener(this);
        destination.addListener(this);
    }

    public void setOn(EditorFx editor){
        this.editor = editor;
        editor.getChildren().add(curve);
        editor.getChildren().add(arrow);
    }

    @Override
    public void geometryChanged(Block block) {
        Point ps = new Point(source.getNode().getLayoutX()+ source.getNode().getWidth()/2, source.getNode().getLayoutY()+ source.getNode().getHeight()/2);
        Point pd = new Point(destination.getNode().getLayoutX()+ destination.getNode().getWidth()/2, destination.getNode().getLayoutY()+ destination.getNode().getHeight()/2);
        Point s = source.getNode().getAnchor(destination.getNode().getLayoutX(), destination.getNode().getLayoutY());
        Point d = destination.getNode().getAnchor(source.getNode().getLayoutX(), source.getNode().getLayoutY());
        double dist = Math.sqrt((ps.x-pd.x)*(ps.x-pd.x) + (ps.y-pd.y)*(ps.y-pd.y))/75;
        curve.setStartX(s.x);
        curve.setStartY(s.y);
        curve.setControlX1(s.x + (s.x - ps.x)*dist);
        curve.setControlY1(s.y + (s.y - ps.y)*dist);
        curve.setEndX(d.x);
        curve.setEndY(d.y);
        curve.setControlX2(d.x + (d.x - pd.x)*dist);
        curve.setControlY2(d.y + (d.y - pd.y)*dist);
        arrow.setCenterX(d.x);
        arrow.setCenterY(d.y);
        //TODO: strzałka
    }

    public void remove(){
        editor.getChildren().removeAll(curve, arrow);
    }

    @Override
    public void removed(Block source) {
        remove();
    }

    @Override
    public void contentChange(Block source) {

    }

    @Override
    public void connectionChanged(Block source) {
        //TODO: usuń
    }
}
