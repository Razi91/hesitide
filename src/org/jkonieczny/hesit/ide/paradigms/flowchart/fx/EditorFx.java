package org.jkonieczny.hesit.ide.paradigms.flowchart.fx;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import org.jkonieczny.fxswing.Point;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Arrow;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.Block;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.BlockNode;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.DecisionBlock;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.InstructionBlock;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Created by jkonieczny on 28.12.14.
 */
public class EditorFx extends AnchorPane {
    Flowchart flow;

    FCanvas canvas;
//    Canvas canvas = new Canvas();

    class Item {
        Node gui;
        Block block;
    }

    final ContextMenu contextMenu = new ContextMenu();
    final Point contextPoint = new Point();

    ArrayList<Block> blocks = new ArrayList<>();
    ObservableList<Block> selected = FXCollections.observableArrayList();
    final Point dragPoint = new Point();

    public EditorFx(Flowchart flow) {
        this.setPrefSize(100000, 100000);
        setStyle("-fx-background-color: white;");
        this.flow = flow;
        addContextMenu();
        this.setOnMouseClicked(onCanvasMouseClicked);
        flow.getBlocks().addListener((ListChangeListener<Block>) c -> {
            while(c.next()) {
                if(c.wasAdded()){
                    c.getAddedSubList().stream().forEach(new Consumer<Block>() {
                        @Override
                        public void accept(Block block) {
                            getChildren().addAll(block.getNode());
                        }
                    });
                }
            }
        });
    }

    private void addContextMenu() {
        //TODO: internacjonalizacja
        MenuItem item1 = new MenuItem("Blok instrukcji");
        item1.setOnAction(e -> {
            InstructionBlock ib = new InstructionBlock(flow);
            addBlock(ib, contextPoint);
        });
        MenuItem item2 = new MenuItem("Blok decyzji");
        item2.setOnAction(e -> {
            DecisionBlock ib = new DecisionBlock(flow);
            addBlock(ib, contextPoint);
        });
        MenuItem item3 = new MenuItem("Blok końca (TODO)");
        item3.setOnAction(e -> {
            DecisionBlock ib = new DecisionBlock(flow);
            addBlock(ib, contextPoint);
        });
        contextMenu.getItems().addAll(item1, item2, item3);
    }

    public void addBlock(Block b, Point at) {
        blocks.add(b);
        Node n = b.getNode();
        n.setOnMousePressed(this.onBlockMousePressed);
        n.setOnMouseDragged(this.onBlockMouseDragged);
        n.setOnMouseClicked(this.onBlockMouseClicked);
        b.setPosition(Math.round(at.x/10)*10, Math.round(at.y/10)*10);
        flow.getBlocks().addAll(b);
    }

    private void deselectAll() {
        for (Block b : selected)
            b.selectedProperty().set(false);
        selected.clear();
    }

    private void select(ArrayList<Block> blocks) {
        deselectAll();
        for (Block b : blocks)
            b.selectedProperty().set(true);
        selected.addAll(blocks);
    }

    private void select(Block block) {
        if (selected.contains(block))
            return;
        block.selectedProperty().set(true);
        selected.add(block);
    }

    EventHandler<MouseEvent> onCanvasMouseClicked = ev -> {
        if (ev.getButton() == MouseButton.SECONDARY) {
            contextPoint.x = ev.getX();
            contextPoint.y = ev.getY();
            contextMenu.show(EditorFx.this, ev.getScreenX(), ev.getScreenY());
        } else if (ev.getButton() == MouseButton.PRIMARY) {
            if (contextMenu.isShowing())
                contextMenu.hide();
            else {
                if (ev.getSource() == EditorFx.this)
                    deselectAll();
            }
        }
    };

    EventHandler<MouseEvent> onBlockMousePressed = ev -> {
        Block n = ((BlockNode) ev.getSource()).getBlock();
        if (!ev.isControlDown()) {
            if (!selected.contains(n)) {
                if (!ev.isShiftDown())
                    deselectAll();
                select(n);
            }
        }
        dragPoint.x = ev.getSceneX();
        dragPoint.y = ev.getSceneY();
    };

    EventHandler<MouseEvent> onBlockMouseClicked = ev -> {
        BlockNode n = (BlockNode) ev.getSource();
        if (ev.isShiftDown()) {
            select(n.getBlock());
        } else if (ev.isControlDown()) {
            //TODO: dodaj połączenie
            if (selected.size() == 1) {
                Block n1 = selected.get(0);
                if (n1 != n.getBlock()) {
                    Arrow arr = n1.setNext(n.getBlock());
                    if(arr != null){
                        deselectAll();
                        select(n1);
                        arr.setOn(this);
                    }
                    else {
                        System.out.println("Połączenie nieudane");
                    }
                }
            }
        } else {
            for (Block b : selected)
                b.selectedProperty().set(false);
            selected.clear();
            selected.add(n.getBlock());
            n.getBlock().selectedProperty().set(true);
        }
        ev.consume();
    };

    EventHandler<MouseEvent> onMouseReleased = ev -> {
        for (Block b : selected) {
            b.invalidate();
        }
        if (ev.isDragDetect()) {
            return;
        }
    };

    EventHandler<MouseEvent> onBlockMouseDragged = ev -> {
        double dx = ev.getSceneX() - dragPoint.x;
        double dy = ev.getSceneY() - dragPoint.y;
        if(dx<10 && dx>-10)
            dx = 0;
        else{
            dragPoint.x = ev.getSceneX() + dx%10;
            dx = Math.round(dx / 10)*10;
        }
        if(dy<10 && dy>-10)
            dy = 0;
        else{
            dragPoint.y = ev.getSceneY() + dx%10;
            dy = Math.round(dy/10)*10;
        }
        for (Block b : selected) {
            b.translate(dx, dy);
            b.invalidate();
        }
    };

}
