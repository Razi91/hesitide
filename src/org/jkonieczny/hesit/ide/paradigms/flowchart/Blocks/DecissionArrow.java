package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import javafx.scene.paint.Color;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Arrow;

/**
 * Created by jkonieczny on 10.01.15.
 */
public class DecissionArrow extends Arrow {

    public DecissionArrow(Block source, Block destination, boolean value) {
        super(source, destination);
        setRouteType(value);
    }

    public void setRouteType(boolean value){
        if(value){
            arrow.setFill(Color.BLACK);
            arrow.setStroke(Color.TRANSPARENT);
        }
        else {
            arrow.setFill(Color.TRANSPARENT);
            arrow.setStroke(Color.BLACK);
        }
    }
}
