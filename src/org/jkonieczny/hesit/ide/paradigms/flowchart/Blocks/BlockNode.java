package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.shape.Shape;
import org.jkonieczny.fxswing.Point;

/**
 * Created by jkonieczny on 08.01.15.
 */
public class BlockNode extends AnchorPane implements BlockListener {

    Block block;
    Label label;
    Shape shape;

    public BlockNode(Block block) {
        this.block = block;
        label = new Label("Block");
        label.setStyle("-fx-border-color: black;" +
                "-fx-border-width: 1px;");
        shape = createShape();
        getChildren().add(shape);
        setBottomAnchor(label, 10.0);
        setTopAnchor(label, 10.0);
        setLeftAnchor(label, 10.0);
        setRightAnchor(label, 10.0);
        getChildren().add(label);
        block.selectedProperty().addListener((observable, oldValue, newValue) -> {
            setSelected(newValue);
        });
        contentChange(block);
        updatePosition();

        ContextMenu menu = new ContextMenu();
        MenuItem remove = new MenuItem("Usuń");
        remove.setOnAction(ev -> {
            block.remove();
        });
        menu.getItems().addAll(remove);
        this.setOnContextMenuRequested(ev ->{
            menu.show(this, ev.getScreenX(), ev.getScreenY());
        });
    }

    protected Shape createShape() {
        Rectangle rect = new Rectangle(0, 0, 0, 0);
        rect.setFill(Color.SILVER);
        rect.setStroke(Color.BLACK);
        rect.setStrokeWidth(1);
        rect.setLayoutX(0);
        rect.setLayoutY(0);
        label.widthProperty().addListener(ev -> {
            rect.setWidth(label.getWidth() + 20);
        });
        label.heightProperty().addListener(ev -> {
            rect.setHeight(label.getHeight() + 20);
        });
        return rect;
    }

    public void invalidate() {
        updatePosition();
    }

    public void translate(double x, double y) {
        this.setLayoutX(this.getLayoutX() + x);
        this.setLayoutY(this.getLayoutY() + y);
    }

    public void updatePosition() {
        this.setLayoutX(block.getPosition().x - getWidth() / 2);
        this.setLayoutY(block.getPosition().y - getHeight() / 2);
    }

    public Point getAnchor(double x, double y) {
        invalidate();
        double dx = x - this.getLayoutX();
        double dy = y - this.getLayoutY();
        double a = Math.atan2(dy, dx) / Math.PI;
        if (-0.75 <= a && a < -0.25) {
            return new Point(getLayoutX() + getWidth() / 2, getLayoutY());
        }
        if (-0.25 <= a && a < 0.25) {
            return new Point(getLayoutX() + getWidth(), getLayoutY() + getHeight() / 2);
        }
        if (0.25 <= a && a < 0.75) {
            return new Point(getLayoutX() + getWidth() / 2, getLayoutY() + getHeight());
        }
        if (0.75 <= a || -0.75 > a) {
            return new Point(getLayoutX(), getLayoutY() + getHeight() / 2);
        }
        return new Point(getLayoutX() + getWidth() / 2, getLayoutY() + getHeight() / 2);
    }

    private void setSelected(boolean set) {
        if (set) {
            shape.setFill(Color.WHITE);
        } else {
            shape.setFill(Color.SILVER);
        }
    }

    public Block getBlock() {
        return block;
    }

    @Override
    public void geometryChanged(Block source) {
        invalidate();
    }

    @Override
    public void removed(Block source) {

    }

    @Override
    public void contentChange(Block source) {
        label.setText(block.getDisplay());
        invalidate();
    }

    @Override
    public void connectionChanged(Block source) {

    }
}
