package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import org.jkonieczny.hesit.ide.paradigms.flowchart.Arrow;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;

/**
 * Created by jkonieczny on 07.01.15.
 */
public class StartBlock extends Block {

    public class Model extends BlockModel {

    }

    Block next;

    public Arrow setNext(Block next) {
        this.next = next;
        Arrow arrow = new Arrow(this, next);
        return arrow;
    }

    @Override
    public boolean allowInto(){
        return false;
    }

    public StartBlock(Flowchart f) {
        super(f, Block.INIT);
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getDisplay() {
        return "Start";
    }

    @Override
    public BlockModel getModel() {
        Model m = new Model();
        m.id = id;
        return m;
    }


    @Override
    public BlockNode getNode() {
        if (node == null) {
            node = new RoundBlockNode(this);
        }
        return node;
    }


}
