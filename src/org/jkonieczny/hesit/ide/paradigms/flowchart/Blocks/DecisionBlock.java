package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import org.jkonieczny.hesit.ide.paradigms.flowchart.Arrow;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;

/**
 * Created by jkonieczny on 07.01.15.
 */
public class DecisionBlock extends Block {


    public class Model extends Block.BlockModel {

    }

    Block onTrue, onFalse;
    DecissionArrow onTrueArrow, onFalseArrow;
    public Arrow setNext(Block next) {
        DecissionArrow arrow = null;
        if (onTrue == null) {
            this.onTrue = next;
            arrow = new DecissionArrow(this, next, true);
            onTrueArrow = arrow;
        }
        else {
            if(this.onFalse == null) {
                this.onFalse = next;
                arrow = new DecissionArrow(this, next, false);
                onFalseArrow = arrow;
            }
            else {
                onFalse = onTrue;
                onFalseArrow.remove();
                onFalseArrow = onTrueArrow;
                onFalseArrow.setRouteType(false);
                this.onTrue = next;
                arrow = new DecissionArrow(this, next, true);
                onTrueArrow = arrow;
            }
        }
        return arrow;
    }


    public DecisionBlock(Flowchart f) {
        super(f, Block.DECISION);
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getDisplay() {
        return "condition";
    }

    @Override
    public BlockModel getModel() {
        Model m = new Model();
        m.id = id;
        return m;
    }


    @Override
    public BlockNode getNode() {
        if (node == null) {
            node = new DecissionBlockNode(this);
        }
        return node;
    }


}
