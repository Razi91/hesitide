package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import org.jkonieczny.hesit.ide.paradigms.flowchart.Arrow;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;
import org.jkonieczny.hesit.ide.paradigms.flowchart.fx.EditorFx;

/**
 * Created by jkonieczny on 07.01.15.
 */
public class InstructionBlock extends Block {

    Block next;

    public class Model extends Block.BlockModel {

    }

    public Block getNext() {
        return next;
    }

    public Arrow setNext(Block next) {
        this.next = next;
        Arrow arrow = new Arrow(this, next);
        return arrow;
    }


    public InstructionBlock(Flowchart f) {
        super(f, Block.INSTRUCTIONS);
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getDisplay() {
        return "code";
    }

    @Override
    public BlockModel getModel() {
        Model m = new Model();
        m.id = id;
        return m;
    }


}
