package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;

/**
 * Created by jkonieczny on 11.01.15.
 */
public class RoundBlockNode extends BlockNode {
    public RoundBlockNode(Block block) {
        super(block);
    }

    @Override
    protected Shape createShape() {
        Ellipse ellipse = new Ellipse();
        ellipse.setFill(Color.SILVER);
        ellipse.setStroke(Color.BLACK);
        ellipse.setStrokeWidth(1);
        ellipse.setLayoutX(0);
        ellipse.setLayoutY(0);
        label.widthProperty().addListener(ev -> {
            ellipse.setCenterX(label.getWidth() / 2 + 10);
            ellipse.setRadiusX(label.getWidth()/2 + 10);
        });
        label.heightProperty().addListener(ev -> {
            ellipse.setCenterY(label.getHeight() / 2 + 10);
            ellipse.setRadiusY(label.getHeight()/2 + 10);
        });
        return ellipse;
    }

    @Override
    public void invalidate() {
    }
}
