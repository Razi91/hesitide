package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import org.jkonieczny.fxswing.Point;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Arrow;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;

import java.util.ArrayList;

/**
 * Created by jkonieczny on 28.12.14.
 */
public abstract class Block {

    protected BlockNode node;
    protected final Flowchart flowchart;
    protected Long id;
    protected final String type;
    ArrayList<BlockListener> listeners = new ArrayList<>();
    protected Point position = new Point();
    protected Point translate = new Point();

    public boolean allowInto(){
        return true;
    }

    public boolean getSelected() {
        return selected.get();
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    protected BooleanProperty selected = new SimpleBooleanProperty();

    public static String INIT = "init";
    public static String TERM = "term";
    public static String INSTRUCTIONS = "instructions";
    public static String DECISION = "decision";

    public abstract class BlockModel {
        public long id;
        public String type;
    }

    public static Block decode(Flowchart f, BlockModel model) {
        return null;
    }

    Block(Flowchart f, String type) {
        this.flowchart = f;
        this.type = type;
    }

    public void translate(double x, double y){
        getNode().translate(x, y);
        translate.x += x;
        translate.y += y;
    }

    public void setPosition(double x, double y){
        this.position.x = Math.round(x/10)*10;
        this.position.y = Math.round(y/10)*10;
        if(node != null)
            node.invalidate();
    }

    public Point getPosition(){
        return position;
    }


    public void addListener(BlockListener listener) {
        listeners.add(listener);
    }


    public void invalidate() {
        this.position.x += translate.x;
        this.position.y += translate.y;
        translate.x = 0;
        translate.y = 0;
        for (BlockListener l : listeners) {
            l.geometryChanged(this);
        }
    }

    public void remove(){

    }

    public abstract Arrow setNext(Block next);

    public abstract String getCode();

    public abstract String getDisplay();

    public abstract BlockModel getModel();

    public BlockNode getNode() {
        if (node == null) {
            node = new BlockNode(this);
        }
        return node;
    }


}
