package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;

/**
 * Created by jkonieczny on 11.01.15.
 */
public class DecissionBlockNode extends BlockNode {
    public DecissionBlockNode(Block block) {
        super(block);
    }

    @Override
    protected Shape createShape() {
        Polygon poly = new Polygon();
        poly.setFill(Color.SILVER);
        poly.setStroke(Color.BLACK);
        poly.setStrokeWidth(1);
        poly.setLayoutX(0);
        poly.setLayoutY(0);
        label.widthProperty().addListener(ev -> {
            poly.getPoints().addAll(
                    0.0, getHeight() / 2,
                    getWidth() / 2, 0.0,
                    getWidth(), getHeight() / 2,
                    getWidth() / 2, getHeight()
            );
        });
        label.heightProperty().addListener(ev -> {
            poly.getPoints().addAll(
                    0.0, getHeight() / 2 + 10,
                    getWidth() / 2 + 10, 0.0,
                    getWidth() + 20, getHeight() / 2 + 10,
                    getWidth() / 2 + 10, getHeight() + 20
            );
        });
        return poly;
    }

    @Override
    public void invalidate() {
    }
}
