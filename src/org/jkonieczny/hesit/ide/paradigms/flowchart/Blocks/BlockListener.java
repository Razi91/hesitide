package org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks;

/**
 * Created by jkonieczny on 08.01.15.
 */

public interface BlockListener {
    public void geometryChanged(Block source);

    public void removed(Block source);

    public void contentChange(Block source);

    public void connectionChanged(Block source);
}
