package org.jkonieczny.hesit.ide.paradigms.js;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import org.jkonieczny.hesit.afs.AFile;
import org.jkonieczny.hesit.ide.paradigms.Paradigm;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Blocks.Block;
import org.jkonieczny.hesit.ide.paradigms.flowchart.fx.EditorFx;

import java.io.*;

/**
 * Created by jkonieczny on 31.12.14.
 */
public class JS extends Paradigm {
    ObservableList<Block> blocks = FXCollections.observableArrayList();

    TextArea editor;
    String code = "";

    public static Paradigm create(AFile file) {
        Paradigm f = new JS(file);
        return f;
    }

    AFile file;

    public JS(AFile file) {
        this.file = file;
        load();
    }

    @Override
    public void save() {
        try {
            file.getOutputStream().write(editor.getText().getBytes());
        } catch (Exception e) {
        }
    }

    @Override
    public void load() {
        try {
            Reader r = new InputStreamReader(file.getInputStream());
            StringWriter sw = new StringWriter();
            char[] buffer = new char[1024];
            for (int n; (n = r.read(buffer)) != -1; )
                sw.write(buffer, 0, n);
            code = sw.toString();
            editor.setText(code);
        } catch (Exception e) {
        }
    }

    @Override
    public Node getEditorFx() {
        if (editor == null)
            this.editor = new TextArea();
        return editor;
    }

    public void closeEditor() {
        editor = null;
    }

    public static String getFileExtension(){
        return "js";
    }
}
