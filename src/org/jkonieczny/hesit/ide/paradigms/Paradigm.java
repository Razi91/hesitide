package org.jkonieczny.hesit.ide.paradigms;

import javafx.scene.Node;

import javax.swing.*;

/**
 * Paradigm is an abstract class that provides all tools for it's paradigm
 */
public abstract class Paradigm {


    public JPanel getEditorSwing() {
        return null;
    }

    public Node getEditorFx() {
        return null;
    }
    public void closeEditor(){

    }

    public abstract void save();
    public abstract void load();

}
