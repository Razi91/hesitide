package org.jkonieczny.hesit.ide.runtime;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 * Created by jkonieczny on 05.10.14.
 */
public class Runner {
    private static Runner instance;
    public static Runner getRunner() {
        if (instance == null)
            instance = new Runner();
        return instance;
    }

    ScriptEngine engine;
    ScriptEngineManager engineManager;

    public Runner() {
    }

    public void reset(){
        engineManager = new ScriptEngineManager();
        engine = engineManager.getEngineByName("nashorn");
        if (engine == null)
            engine = engineManager.getEngineByExtension("js");

    }

    public ScriptEngine getEngine(){
        return engine;
    }
}
