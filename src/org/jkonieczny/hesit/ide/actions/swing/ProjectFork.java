package org.jkonieczny.hesit.ide.actions.swing;

import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class ProjectFork extends AbstractAction {

    public ProjectFork() {
        super(I18n.getString("project.fork"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
}
