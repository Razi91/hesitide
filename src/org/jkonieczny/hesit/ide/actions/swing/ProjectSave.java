package org.jkonieczny.hesit.ide.actions.swing;

import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class ProjectSave extends AbstractAction {

    public ProjectSave() {
        super(I18n.getString("project.save"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
}
