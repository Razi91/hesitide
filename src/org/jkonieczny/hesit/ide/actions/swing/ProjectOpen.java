package org.jkonieczny.hesit.ide.actions.swing;

import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class ProjectOpen extends AbstractAction {

    public ProjectOpen() {
        super(I18n.getString("project.open"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
