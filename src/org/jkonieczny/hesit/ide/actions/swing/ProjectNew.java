package org.jkonieczny.hesit.ide.actions.swing;

import org.jkonieczny.hesit.ide.gui.swing.dialogs.NewProject;
import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class ProjectNew extends AbstractAction {
    public ProjectNew(){
        super(I18n.getString("project.new"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SwingUtilities.invokeLater(()->{
            new NewProject().setVisible(true);
        });
    }
}
