package org.jkonieczny.hesit.ide.actions.swing;

import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class Exit extends AbstractAction {

    public Exit() {
        super(I18n.getString("menu.exit"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //TODO: save?
        System.exit(0);
    }
}
