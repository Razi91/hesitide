package org.jkonieczny.hesit.ide.actions.fx;

import javafx.event.ActionEvent;
import org.jkonieczny.fxswing.ActionFX;
import org.jkonieczny.hesit.ide.utils.I18n;

import java.util.HashMap;

/**
 * Created by jkonieczny on 03.10.14.
 */
public class Actions {
    private static Actions instance;
    HashMap<String, ActionFX> actions;

    public static Actions getInstance(){
        if (instance == null) instance = new Actions();
        return instance;
    }

    public ActionFX getAction(String name){
        return getInstance().actions.get(name);
    }

    public static void add(ActionFX action){
        getInstance().actions.put(action.name, action);
    }

    private Actions() {
        actions = new HashMap<>();
    }

    private void loadDefaultActions(){

    }

}

class ProjectNew extends ActionFX {
    protected ProjectNew() {
        super("project.new", I18n.getString("menu.file.new"));
    }

    @Override
    public void handle(ActionEvent event) {

    }
}

class ProjectOpen extends ActionFX {
    protected ProjectOpen() {
        super("project.open", I18n.getString("menu.file.new"));
    }

    @Override
    public void handle(ActionEvent event) {

    }
}

class ProjectSave extends ActionFX {
    protected ProjectSave() {
        super("project.save", I18n.getString("menu.file.new"));
    }

    @Override
    public void handle(ActionEvent event) {

    }
}

class Exit extends ActionFX {
    protected Exit() {
        super("application.exit", I18n.getString("menu.file.new"));
    }

    @Override
    public void handle(ActionEvent event) {
        System.exit(0);
    }
}
