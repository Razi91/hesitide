package org.jkonieczny.hesit.ide.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * Created by jkonieczny on 30.09.14.
 */
public final class I18n {

    static {
        new I18n();
    }

    private static I18n instance;

    private ResourceBundle source;


    private List<ResourceBundle> sources;

    private I18n() {
        source = ResourceBundle.getBundle("org.jkonieczny.hesit.ide.assets.translation", java.util.Locale.getDefault(), new UTF8Control());
    }

    public static void addSource(String path, ClassLoader classLoader){
        ResourceBundle bundle = ResourceBundle.getBundle(path, java.util.Locale.getDefault(), classLoader, new UTF8Control());
        getInstance().sources.add(bundle);
    }

    private static I18n getInstance(){
        if (instance == null) instance = new I18n();
        return instance;
    }

    /**
     * Get localized string
     *
     * @param key
     * @return
     */
    public static String getString(final String key) {
        if (instance == null) instance = new I18n();
        if (instance.source.containsKey(key))
            return instance.source.getString(key);

        return null;
    }

    public static ResourceBundle getBundle(){
        return getInstance().source;
    }
}

class UTF8Control extends ResourceBundle.Control {
    @Override
    public ResourceBundle newBundle
            (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {
        String bundleName = toBundleName(baseName, locale);
        String resourceName = toResourceName(bundleName, "properties");
        ResourceBundle bundle = null;
        InputStream stream = null;
        if (reload) {
            URL url = loader.getResource(resourceName);
            if (url != null) {
                URLConnection connection = url.openConnection();
                if (connection != null) {
                    connection.setUseCaches(false);
                    stream = connection.getInputStream();
                }
            }
        } else {
            stream = loader.getResourceAsStream(resourceName);
        }
        if (stream != null) {
            try {
                // Only this line is changed to make it to read properties files as UTF-8.
                bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
            } finally {
                stream.close();
            }
        }
        return bundle;
    }
}