package org.jkonieczny.hesit.ide.project;

import org.jkonieczny.hesit.ide.paradigms.Paradigm;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class ProjectItem {
    Paradigm paradigm;
    String path;
    String name;
    String type;

    public ProjectItem(Paradigm paradigm, String path, String name, String type) {
        this.paradigm = paradigm;
        this.path = path;
        this.name = name;
        this.type = type;
    }

    public boolean isParadigm() {
        return paradigm != null;
    }

    public Paradigm getParadigm() {
        return paradigm;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return name + "." + type;
    }
}
