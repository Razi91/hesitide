package org.jkonieczny.hesit.ide.project;

import javafx.collections.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import org.jkonieczny.hesit.afs.AFS;
import org.jkonieczny.hesit.afs.AFile;
import org.jkonieczny.hesit.afs.binary.Binary;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;
import org.jkonieczny.hesit.ide.paradigms.Paradigm;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;
import org.jkonieczny.hesit.ide.paradigms.js.JS;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by jkonieczny on 30.09.14.
 * This class should be divided into GUI-independent manager and GUI interface
 */
public class Project {

    private ObservableList<ProjectItem> projectItems;
    private AFS fs;
    private Meta meta;

    IDEWorkspaceFx ideWorkspaceFx;

    private Project(IDEWorkspaceFx ideWorkspaceFx) {
        this.ideWorkspaceFx = ideWorkspaceFx;
        projectItems = FXCollections.observableArrayList();
        meta = new Meta();
        projectItems.addListener(ideWorkspaceFx.getProjectTree().getProjectListener());
    }


    public static Project defaultProject(IDEWorkspaceFx ideWorkspaceFx) {
        Project p = new Project(ideWorkspaceFx);
        p.fs = new Binary("/tmp/hesit" + (System.currentTimeMillis() % 10000) + ".hst");
        p.fs.save();

        return p;
    }

    private class Meta {
        private String name;
        private String[] items;
        private String[] authors;
    }

    private void scanFs() {
        List<ProjectItem> projectItems = new ArrayList<>();
        for (AFile file : fs.getFiles()) {
            String name[] = file.getFilename().split("\\.");
            switch (name[1]) {
                case "flow": {
                    Flowchart p = new Flowchart(file);
                    ProjectItem projectItem = new ProjectItem(p, "/", name[0], name[1]);
                    projectItems.add(projectItem);
                    break;
                }
                case "js": {
                    JS p = new JS(file);
                    ProjectItem projectItem = new ProjectItem(p, "/", name[0], name[1]);
                    projectItems.add(projectItem);
                    break;
                }
            }
        }
        this.projectItems.setAll(projectItems);
    }

    public static class Creator {
        private Project project;

        public Creator() {
            project = new Project(IDEWorkspaceFx.getIDEWorkspace());
        }

        public Creator setFS(AFS fs) {
            project.fs = fs;
            return this;
        }

        public Creator setName(String name) {
            project.meta.name = name;
            return this;
        }

        public boolean set() {
            return true;
        }
    }

    public List<ProjectItem> getProjectItems() {
        return projectItems;
    }

    public void addItem(ProjectItem projectItem) {
        projectItems.add(projectItem);
    }

    public static void open() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showOpenDialog(IDEWorkspaceFx.getIDEWorkspace().getStage());
        if (file != null) {
            Project p = new Project(IDEWorkspaceFx.getIDEWorkspace());
            p.fs = new Binary(file.getAbsolutePath());
            p.load();
            IDEWorkspaceFx.getIDEWorkspace().setProject(p);
        }
    }

    public void save() {
        for(ProjectItem projectItem: projectItems) {
            projectItem.getParadigm().save();
        }
        fs.save();
    }

    public void saveAs() {
        File file = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        file = fileChooser.showSaveDialog(IDEWorkspaceFx.getIDEWorkspace().getStage());
        if (file != null) {
            fs.saveAs(file);
        }
    }

    public void load() {
        fs.load();
        scanFs();
    }

    /**
     * Adds new paradigm to project
     * MUST BE EXPORTED SOMEWHERE ELSE!
     *
     * @param paradigm Paradigm's class
     */
    public void addParadigm(Class<? extends Paradigm> paradigm) {
        //TODO: przenieść tą funkcję gdzie indziej
        try {
            Method create = paradigm.getMethod("create", AFile.class);
            String extension = (String) paradigm.getMethod("getFileExtension").invoke(null);
            //TODO: internacjonalizacja
            AFile file;
            TextInputDialog dialog = new TextInputDialog("");
            dialog.setTitle("Nowy dokument");
            dialog.setHeaderText("Utwórz nowy dokument");
            dialog.setContentText("Podaj nazwę dokumentu");
            while (true) {
                Optional<String> result = dialog.showAndWait();
                if (!result.isPresent()) {
                    return;
                }
                String name = result.get();
                try {
                    file = fs.createFile("", result.get() + "." + extension);
                    if (file != null) {
                        Paradigm newParadigm = (Paradigm) create.invoke(null, file);
                        this.addItem(new ProjectItem(newParadigm, file.getPath(), name, "flow"));
                    }
                    break;
                } catch (FileAlreadyExistsException e) {
                    //TODO: Internacjonalizacja
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Plik istnieje");
                    alert.setHeaderText("Plik już istnieje");
                    alert.setContentText("Wybrać inny, wybrać inną nazwę?");
                    Optional<ButtonType> resultExist = alert.showAndWait();
                    if (resultExist.get() == ButtonType.OK) {
                        continue;
                    } else {
                        return;
                    }
                }
            }
        } catch (IllegalAccessException e) {
            //TODO: internacjonalizacja
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Brak dostępu");
            alert.setHeaderText("Brak dostępu");
            alert.setContentText("Brak praw dostępu do tego katalogu");
            alert.showAndWait();
        } catch (InvocationTargetException e) {
            //TODO: dodać ostrzeżeniem
        } catch (NoSuchMethodException e) {
            //TODO: internacjonalizacja
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Nowy dokument");
            alert.setHeaderText("Niepełna implementacja");
            alert.setContentText("Wybrany paradygmat jest niepoprawnie zaimplementowany");
            alert.showAndWait();
        }
    }


}
