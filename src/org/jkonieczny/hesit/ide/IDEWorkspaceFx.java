package org.jkonieczny.hesit.ide;

import javafx.stage.Stage;
import org.jkonieczny.hesit.ide.gui.fx.ConsoleFx;
import org.jkonieczny.hesit.ide.gui.fx.MainWindowFx;
import org.jkonieczny.hesit.ide.gui.fx.ProjectTreeFx;
import org.jkonieczny.hesit.ide.gui.fx.WorkspaceFx;
import org.jkonieczny.hesit.ide.project.Project;
import org.jkonieczny.hesit.runtime.Environment;

/**
 * Created by jkonieczny on 05.10.14.
 */
public class IDEWorkspaceFx {
    static IDEWorkspaceFx instance;

    public static IDEWorkspaceFx getIDEWorkspace() {
        return instance;
    }

    ConsoleFx console;

    public ConsoleFx getConsole() {
        if (console == null) {
            console = new ConsoleFx(this);
        }
        return console;
    }

    Project project;

    public Project getProject() {
        if (project == null) {
            project = Project.defaultProject(this);
        }
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    Environment environment;

    public Environment getEnvironment() {
        if (environment == null) {
            environment = new Environment(this);
        }
        return environment;
    }

    WorkspaceFx workspace;

    public WorkspaceFx getWorkspace() {
        if (workspace == null) {
            workspace = new WorkspaceFx(this);
        }
        return workspace;
    }

    ProjectTreeFx projectTree;

    public ProjectTreeFx getProjectTree() {
        if (projectTree == null) {
            projectTree = new ProjectTreeFx(this);
        }
        return projectTree;
    }

    Stage stage;

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public IDEWorkspaceFx() {
    }

    public static void main(String args[]) {
        instance = new IDEWorkspaceFx();
        MainWindowFx.startApp(args);
    }
}
