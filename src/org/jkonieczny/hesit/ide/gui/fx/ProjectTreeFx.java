package org.jkonieczny.hesit.ide.gui.fx;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;
import org.jkonieczny.hesit.ide.project.ProjectItem;
import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.io.IOException;


/**
 * Created by jkonieczny on 27.12.14.
 */
public class ProjectTreeFx implements INode {
    @FXML
    TreeView<ProjectItem> tree;
    Node root;

    IDEWorkspaceFx ideWorkspaceFx;

    public ProjectTreeFx(IDEWorkspaceFx ideWorkspaceFx) {
        this.ideWorkspaceFx = ideWorkspaceFx;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(I18n.getBundle());
            loader.setController(this);
            root = loader.load(getClass().getResource("ProjectTree.fxml").openStream());
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "ERROR");
            System.exit(1);
        }
        tree.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    TreeItem<ProjectItem> item = tree.getSelectionModel().getSelectedItem();
                    if(item != null) {
                        ideWorkspaceFx.getWorkspace().open(item.getValue());
                    }
                }
                else if(event.getButton()== MouseButton.SECONDARY){
                    TreeItem<ProjectItem> item = tree.getSelectionModel().getSelectedItem();

                }
            }
        });
    }

    /**
     * Update ProjectTree
     * @param projectItems
     * @param change
     */
    public void update(ObservableList<ProjectItem> projectItems, ListChangeListener.Change<ProjectItem> change) {
        TreeItem treeRoot = new TreeItem<String>("Projekt");
        for (ProjectItem projectItem : projectItems) {
            System.out.println("in project: " + projectItem.getName());
            TreeItem treeItem = new TreeItem<>(projectItem);
            treeRoot.getChildren().add(treeItem);
        }
        tree.setRoot(treeRoot);
        treeRoot.setExpanded(true);
        tree.setShowRoot(false);
    }


    @Override
    public Node getNode() {
        return root;
    }

    public ListChangeListener getProjectListener() {
        return projectListener;
    }

    ListChangeListener projectListener = c -> {
        update(c.getList(), c);
    };
}
