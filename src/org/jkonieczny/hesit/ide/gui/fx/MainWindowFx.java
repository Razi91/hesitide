package org.jkonieczny.hesit.ide.gui.fx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.jkonieczny.hesit.ide.gui.fx.elements.Toolbar;
import org.jkonieczny.hesit.ide.utils.I18n;
import org.jkonieczny.hesit.ide.Workspace;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;
import org.jkonieczny.hesit.ide.gui.Console;
import org.jkonieczny.hesit.ide.gui.MainWindow;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class MainWindowFx extends Application implements MainWindow {

    @FXML
    private MenuItem menuFileNew;
    @FXML
    private TableView properties;
    @FXML
    private TabPane tabs;
    @FXML
    private HBox consoleNode;
    @FXML
    private Pane workspace;
    @FXML
    private Pane projectTreeNode;
    @FXML
    private Pane main;
    @FXML
    private TabPane toolbar;

    Stage stage;

    Parent root = null;

    Console console;

    private void init(Stage stage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(I18n.getBundle());
            loader.setController(this);
            root = loader.load(getClass().getResource("MainWindow.fxml").openStream());
            Scene scene = new Scene(root, 1000, 800);
            stage.setTitle("Hesit IDE");
            stage.setScene(scene);
            Platform.runLater(() -> installModules());
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "ERROR");
            System.exit(1);
        }
    }

    private void installModules() {
        consoleNode.getChildren().addAll(IDEWorkspaceFx.getIDEWorkspace().getConsole().getNode());
        workspace.getChildren().addAll(IDEWorkspaceFx.getIDEWorkspace().getWorkspace().getNode());
        projectTreeNode.getChildren().addAll(IDEWorkspaceFx.getIDEWorkspace().getProjectTree().getNode());
        //main.getChildren().add(1, new Toolbar(IDEWorkspaceFx.getIDEWorkspace()).getNode());
        new Toolbar(IDEWorkspaceFx.getIDEWorkspace(), toolbar);
    }


    @Override
    public void start(Stage stage) throws Exception {
        init(stage);
        this.stage = stage;
        IDEWorkspaceFx.getIDEWorkspace().setStage(stage);
        stage.show();
        Workspace.init(this);
    }

    public static void startApp(String args[]) {
        launch(args);
    }

}
