package org.jkonieczny.hesit.ide.gui.fx;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;
import org.jkonieczny.hesit.ide.utils.I18n;
import org.jkonieczny.hesit.ide.gui.Console;

import javax.script.ScriptException;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by jkonieczny on 05.10.14.
 */
public class ConsoleFx extends Console  implements INode {
    @FXML
    private TextArea consoleView;
    @FXML
    private TextField consoleInput;
    private ConsoleInterface iface;

    Parent root;

    IDEWorkspaceFx ideWorkspace;

    @Override
    public Node getNode() {
        return root;
    }

    public abstract class ConsoleInterface {
        public abstract void print(String message, int flags);
    }

    public ConsoleFx(IDEWorkspaceFx ideWorkspace){
        this.ideWorkspace = ideWorkspace;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(I18n.getBundle());
            loader.setController(this);
            root = loader.load(getClass().getResource("Console.fxml").openStream());
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "ERROR");
            System.exit(1);
        }
    }

    @FXML
    private void onEnter(){
        try {
            ideWorkspace.getEnvironment().execute(consoleInput.getText());
            consoleInput.clear();
        } catch (ScriptException e) {
            //TODO: printować do konsoli
            e.printStackTrace();
        }
    }


    @Override
    public void clear() {

    }

    @Override
    public void print(String msg, Format format, int flags) {
        consoleView.appendText(msg + "\n");
    }
}
