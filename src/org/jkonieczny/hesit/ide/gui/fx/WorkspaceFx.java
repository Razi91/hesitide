package org.jkonieczny.hesit.ide.gui.fx;

import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;
import org.jkonieczny.hesit.ide.project.ProjectItem;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jkonieczny on 27.12.14.
 */
public class WorkspaceFx implements INode {

    TabPane root;


    Map<Tab, ProjectItem> tabsToItems;
    Map<ProjectItem, Tab> itemsToTabs;

    IDEWorkspaceFx ideWorkspaceFx;

    public WorkspaceFx(IDEWorkspaceFx ideWorkspaceFx) {
        this.ideWorkspaceFx = ideWorkspaceFx;
        root = new TabPane();
        root.setTabClosingPolicy(TabClosingPolicy.SELECTED_TAB);
        root.getSelectionModel().selectedItemProperty().addListener(this.tabChangeListener);
        root.getTabs().addListener(tabListener);
        root.setPrefWidth(1000000);
        insertStartTab();
        itemsToTabs = new HashMap<>();
        tabsToItems = new HashMap<>();
    }

    private void insertStartTab() {
        Tab tab = new Tab("Help");
        tab.setStyle("background: red;");
        tab.setContent(new Label("Hello, placeholder"));
        tab.setClosable(false);
        root.getTabs().add(tab);
    }

    public void open(ProjectItem projectItem) {
        if (itemsToTabs.containsKey(projectItem)) {
            root.getSelectionModel().select(itemsToTabs.get(projectItem));
            return ;
        }
        Tab tab = new Tab();
        tab.setContent(projectItem.getParadigm().getEditorFx());
        tab.setText(projectItem.getName());
        root.getTabs().add(tab);
        root.getSelectionModel().select(tab);
        tabsToItems.put(tab, projectItem);
        itemsToTabs.put(projectItem, tab);
        //Scene scene = new Scene(projectItem.getParadigm().getEditorFx());
    }

    @Override
    public Node getNode() {
        return root;
    }

    ChangeListener<Tab> tabChangeListener = (observable, oldValue, newValue) -> {
        //TODO: zmiana panelu narzędziowego
    };

    ListChangeListener<Tab> tabListener = c -> {
        while (c.next()) {
            if (c.wasRemoved()) {
                c.getRemoved().stream().filter(tabsToItems::containsKey).forEach(tab -> {
                    ProjectItem projectItem = tabsToItems.get(tab);
                    tabsToItems.remove(tab);
                    itemsToTabs.remove(projectItem);
                });
            }
        }
    };

}
