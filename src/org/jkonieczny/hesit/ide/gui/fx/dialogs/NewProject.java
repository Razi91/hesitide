package org.jkonieczny.hesit.ide.gui.fx.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.jkonieczny.hesit.ide.utils.I18n;

import java.io.IOException;

/**
 * Created by jkonieczny on 10.11.14.
 */
public class NewProject {
    public void showDialog(){
        Parent root = null;
        Stage stage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(I18n.getBundle());
            loader.setController(this);
            root = loader.load(getClass().getResource("NewProject.fxml").openStream());
            Scene scene = new Scene(root, 300, 275);
            stage.setTitle(I18n.getString("project.new"));
            stage.setScene(scene);
        } catch(IOException exception) {

        }
    }
}
