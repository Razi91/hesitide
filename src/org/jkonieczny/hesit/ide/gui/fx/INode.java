package org.jkonieczny.hesit.ide.gui.fx;

import javafx.scene.Node;

/**
 * Created by jkonieczny on 27.12.14.
 */
public interface INode {
    public Node getNode();
}
