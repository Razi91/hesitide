package org.jkonieczny.hesit.ide.gui.fx.elements;


import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import org.jkonieczny.hesit.ide.gui.fx.INode;
import org.jkonieczny.hesit.ide.utils.I18n;

/**
 * Created by jkonieczny on 03.10.14.
 */
public class MainMenuBar extends MenuBar implements INode {
    public MainMenuBar() {
        file();
    }

    private void file(){
        Menu file = new Menu(I18n.getString("menu.file"));
        this.getMenus().add(file);
        //menu.getItems().add(newItem);
    }

    @Override
    public Node getNode() {
        return this;
    }
}
