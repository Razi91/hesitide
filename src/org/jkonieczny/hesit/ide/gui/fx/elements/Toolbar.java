package org.jkonieczny.hesit.ide.gui.fx.elements;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.controlsfx.tools.Borders;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;
import org.jkonieczny.hesit.ide.gui.fx.INode;
import org.jkonieczny.hesit.ide.paradigms.flowchart.Flowchart;
import org.jkonieczny.hesit.ide.paradigms.js.JS;
import org.jkonieczny.hesit.ide.project.Project;

/**
 * Created by jkonieczny on 28.12.14.
 */
public class Toolbar{
    Node root;

    IDEWorkspaceFx ideWorkspaceFx;

    public Toolbar(IDEWorkspaceFx ideWorkspaceFx, TabPane toolbar){
        this.ideWorkspaceFx = ideWorkspaceFx;
        toolbar.getTabs().addAll(getFile());
        //root = pane;
    }

    private Tab getFile(){
        //TODO: internacjonalizacja
        Tab t = new Tab("Projekt");
        HBox project = new HBox();

        HBox box1 = new HBox();

        Button newProject = new Button("Nowy");
        newProject.setOnAction(event -> {
            //ideWorkspaceFx.getProject().addParadigm(Flowchart.class);
        });
        Button openProject = new Button("Otwórz");
        openProject.setOnAction(event -> {
            Project.open();
        });
        Button saveProject = new Button("Zapisz");
        saveProject.setOnAction(event -> {
            ideWorkspaceFx.getProject().save();
        });
        Button saveAsProject = new Button("Zapisz jako");
        saveAsProject.setOnAction(event -> {
            ideWorkspaceFx.getProject().saveAs();
        });
        Button newFlow = new Button("Dodaj flow");
        newFlow.setOnAction(event -> {
            ideWorkspaceFx.getProject().addParadigm(Flowchart.class);
        });
        Button newScript = new Button("Dodaj flow");
        newScript.setOnAction(event -> {
            ideWorkspaceFx.getProject().addParadigm(JS.class);
        });

        box1.getChildren().addAll(newProject, saveProject, saveAsProject, openProject);
        Node projectManage = Borders.wrap(box1).lineBorder().title("Plik").buildAll();
        HBox box2 = new HBox();
        box2.getChildren().addAll(newFlow, newScript);

        Node paradigmManage = Borders.wrap(box2).lineBorder().title("Projekt").buildAll();

        project.getChildren().addAll(projectManage, paradigmManage);
        t.setContent(project);
        return t;
    }

}
