package org.jkonieczny.hesit.ide.gui.swing.dialogs;

import org.jkonieczny.hesit.afs.AFS;
import org.jkonieczny.hesit.afs.binary.Binary;
import org.jkonieczny.hesit.ide.project.Project;
import org.jkonieczny.hesit.ide.utils.I18n;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class NewProject extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField projectName;
    private JComboBox storageMethod;
    private JTextField path;

    public NewProject() {
        //super(Workspace.getIDEWorkspace().getWindow());
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        initUI();
        pack();
    }

    private void initUI(){
        //TODO: różne metody!
        this.storageMethod.addItem(I18n.getString("project.storage.file"));
        this.path.setText("file://"+System.getProperty("user.home")+"/Hesit/project.hesit");
    }

    private void onOK() {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        File f = new File(path.getText());
        try {
            URI uri = f.toURI();
            System.out.println(uri.getScheme());
            AFS fs = new Binary("/tmp/binary.kryo");
            boolean created = new Project.Creator()
                    .setName(projectName.getText())
                    .setFS(fs)
                    .set();
            if (created)
                dispose();
        } catch(Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

    private void onCancel() {
        dispose();
    }

}
