package org.jkonieczny.hesit.ide.gui.swing;

import org.jkonieczny.hesit.ide.utils.I18n;
import org.jkonieczny.hesit.ide.actions.swing.Exit;
import org.jkonieczny.hesit.ide.actions.swing.ProjectNew;
import org.jkonieczny.hesit.ide.actions.swing.ProjectOpen;
import org.jkonieczny.hesit.ide.actions.swing.ProjectSave;
import org.jkonieczny.hesit.ide.gui.Console;

import javax.swing.*;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class MainWindow extends JFrame implements org.jkonieczny.hesit.ide.gui.MainWindow {

    private WindowContent content;

    private static void setLAF() {
        try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (Exception e) {

        }
    }

    public MainWindow() {
        setLAF();
        this.setTitle(I18n.getString("window.title"));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setJMenuBar(this.buildMenu());
        content = new WindowContent();
        this.setContentPane(content.form);

        this.setSize(800,600);
    }

    private JMenuBar buildMenu(){
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu(I18n.getString("menu.file"));
        menuBar.add(file);
        JMenuItem fileNewProject = new JMenuItem(new ProjectNew());
        JMenuItem fileOpenProject = new JMenuItem(new ProjectOpen());
        JMenuItem fileSaveProject = new JMenuItem(new ProjectSave());
        file.add(fileNewProject);
        file.add(fileOpenProject);
        file.add(fileSaveProject);
        JMenuItem fileExit = new JMenuItem(new Exit());
        file.addSeparator();
        file.add(fileExit);

        JMenu edit = new JMenu(I18n.getString("menu.edit"));
        menuBar.add(edit);
        JMenu view = new JMenu(I18n.getString("menu.view"));
        menuBar.add(view);
        JMenu help = new JMenu(I18n.getString("menu.help"));
        menuBar.add(help);
        return menuBar;
    }

    public Console getConsole() {
        return null;
    }

    public void closeDocument() {

    }

    public void closeAll() {

    }
}
