package org.jkonieczny.hesit.ide.gui.swing;

import javax.swing.*;

/**
 * Created by jkonieczny on 30.09.14.
 */
public class WindowContent {
    private JTabbedPane workspaceTabs;
    private JTree tree1;
    private JTable table1;
    private JTabbedPane tabbedPane2;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JTextArea console;
    private JTextField consoleInput;
    public JSplitPane form;
}
