package org.jkonieczny.hesit.ide.gui;

/**
 * Created by jkonieczny on 05.10.14.
 */
public abstract class Console {
    public enum Format {
        TEXT,
        ARRAY,
        JSON,
    }

    public static final int WARNING = 1;
    public static final int ERROR = 1<<1;
    public static final int LOCALIZED = 1<<2;

    public abstract void clear();
    public abstract void print(String msg, Format format, int flags);
}
