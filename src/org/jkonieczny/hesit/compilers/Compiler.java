package org.jkonieczny.hesit.compilers;

import org.jkonieczny.hesit.language.Code;
import org.jkonieczny.hesit.language.If;
import org.jkonieczny.hesit.language.Function;
import org.jkonieczny.hesit.language.Instructions;

/**
 * Created by jkonieczny on 05.01.15.
 */
public abstract class Compiler {
    protected StringBuilder sb = new StringBuilder();

    public String getCode() {
        return sb.toString();
    }

    public abstract void compileCode(Code code);

    public abstract void compile(If anIf);
    public abstract void compile(Instructions instructions);
    public abstract void compile(Function function);
}
