package org.jkonieczny.hesit.compilers;

import org.jkonieczny.hesit.language.Code;
import org.jkonieczny.hesit.language.If;
import org.jkonieczny.hesit.language.Function;
import org.jkonieczny.hesit.language.Instructions;

import java.util.ArrayList;

/**
 * Created by jkonieczny on 05.01.15.
 */
public class JavaScript extends Compiler {

    private String intent = "";

    private void insertLine(String code){
        sb.append(intent + code + "\n");
    }

    private void compile(ArrayList<Code> codes){
        String old = intent;
        intent += "  ";
        for(Code c: codes){
            compileCode(c);
        }
        intent = old;
    }
    @Override
    public void compile(If anIf) {
        boolean trueLoop = false;//anIf.isLoop(true);
        boolean falseLoop = false;//anIf.isLoop(false);
        if(trueLoop && falseLoop){
            insertLine("__loop_"+ anIf.getId()+": while(true)}{");
            String old = intent;
            intent += "  ";
            insertLine("if("+ anIf.getId()+") {");
            compile(anIf.getOn(true));
            insertLine("}");
            insertLine("else {");
            compile(anIf.getOn(false));
            insertLine("}");
            insertLine("}");
            intent = old;
        }
        else{
            if(anIf.getOnTrue().size() > 0){
                if(trueLoop)
                    insertLine("while("+ anIf.getId()+") {");
                else
                    insertLine("if("+ anIf.getId()+") {");
                compile(anIf.getOn(true));
                insertLine("}");
            }
            if(anIf.getOnFalse().size() > 0){
                if(falseLoop)
                    insertLine("while(!("+ anIf.getId()+")) {");
                else {
                    if(anIf.getOnTrue().size()>0 && !trueLoop)
                        insertLine("else {");
                    else
                        insertLine("if(!(" + anIf.getId() + ")) {");
                }
                compile(anIf.getOn(false));
                insertLine("}");
            }
        }
    }

    @Override
    public void compile(Instructions instructions){
        insertLine("" + instructions.getId());
    }

    @Override
    public void compileCode(Code code){
        if(code instanceof Instructions)
            compile((Instructions) code);
        else if(code instanceof If)
            compile((If) code);
    }

    @Override
    public void compile(Function function) {
        for(Code c:function.getCodes()){
            compileCode(c);
        }
    }
}
