package org.jkonieczny.hesit.runtime;

import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.jkonieczny.hesit.ide.IDEWorkspaceFx;

import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by jkonieczny on 27.12.14.
 */
public class Environment {
    NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
    NashornScriptEngine base;
    public Environment(IDEWorkspaceFx workspace){
        base = (NashornScriptEngine) factory.getScriptEngine();
        base.put("workspace", workspace);
        try {
            base.eval( new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("RuntimeLibrary.js"))));
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    public void put(String name, Object object){
        base.put(name, object);
    }

    public Object executeBase(String code) throws ScriptException {
        return base.eval(code);
    }

    public Object execute(String code) throws ScriptException {
        return base.eval(code);
    }


}
