var console, print;

console = workspace.getConsole();

print = function(str) {
    return console.print(str, null, 0);
};
