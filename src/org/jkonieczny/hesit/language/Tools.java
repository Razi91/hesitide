package org.jkonieczny.hesit.language;

import java.util.ArrayList;

/**
 * Created by jkonieczny on 06.01.15.
 */
public class Tools {
    public static ArrayList<Code> precompile(ArrayList<Code> codes) {
        ArrayList<Code> opti = new ArrayList<>();
        for (Code c : codes) {
            if (c instanceof If) {
                If cond = (If) c;
                cond.setOn(true, precompile(cond.getOn(true)));
                cond.setOn(false, precompile(cond.getOn(false)));
                opti.add(cond);
                opti.addAll(((If) c).getSharedEnd());
            } else {
                opti.add(c);
            }
        }
        return opti;
    }
}
