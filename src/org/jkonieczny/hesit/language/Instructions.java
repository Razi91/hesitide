package org.jkonieczny.hesit.language;

import org.jkonieczny.hesit.language.instructions.Instruction;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jkonieczny on 29.12.14.
 */
public class Instructions extends Code {
    ArrayList<Instruction> instructions;

    public Instructions(long id, ArrayList<Instruction> instructions) {
        super(id);
        this.instructions = instructions;
    }

    public Instructions(long id, Instruction... instructions) {
        super(id);
        this.instructions = new ArrayList<>(Arrays.asList(instructions));
    }

    public ArrayList<Instruction> getInstructions() {
        return instructions;
    }
}
