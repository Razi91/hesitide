package org.jkonieczny.hesit.language;

import org.jkonieczny.hesit.language.instructions.Condition;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jkonieczny on 29.12.14.
 */
public class If extends Code {

    ArrayList<Code> onTrue;
    ArrayList<Code> onFalse;
    Condition condition;

    public Condition getCondition() {
        return condition;
    }


    public If(long id, Condition condition, ArrayList<Code> onTrue, ArrayList<Code> onFalse) {
        super(id);
        this.onTrue = onTrue;
        this.onFalse = onFalse;
        this.condition = condition;
    }

    public If(long id, Condition condition) {
        super(id);
        this.onTrue = new ArrayList<>();
        this.onFalse = new ArrayList<>();
    }

    public void setOn(boolean val, ArrayList<Code> codes) {
        if (val)
            onTrue = codes;
        else
            onFalse = codes;
    }

    public boolean endsAt(Code block) {
        if (onTrue.size() > 0 && onTrue.get(onTrue.size() - 1).equals(block))
            return true;
        if (onFalse.size() > 0 && onFalse.get(onFalse.size() - 1).equals(block))
            return true;
        boolean ot = onTrue.stream().filter(c -> c instanceof If).anyMatch(c -> ((If) c).endsAt(this));
        boolean of = onFalse.stream().filter(c -> c instanceof If).anyMatch(c -> ((If) c).endsAt(this));
        return ot || of;
    }

    public void addOn(boolean val, ArrayList<Code> codes) {
        (val ? onTrue : onFalse).addAll(codes);
    }

    public void addOn(boolean val, Code... codes) {
        (val ? onTrue : onFalse).addAll(Arrays.asList(codes));
    }

    public ArrayList<Code> getOn(boolean val) {
        return val ? onTrue : onFalse;
    }

    public ArrayList<Code> getOnTrue() {
        return onTrue;
    }

    public ArrayList<Code> getOnFalse() {
        return onFalse;
    }

    /**
     * Clears and return shared code on both ends
     *
     * @return
     */
    public ArrayList<Code> getSharedEnd() {
        ArrayList<Code> shared = new ArrayList<>();
        int i, j;
        i = onTrue.size() - 1;
        j = onFalse.size() - 1;
        while (onTrue.get(i).equals(onFalse.get(j))) {
            shared.add(onFalse.get(j));
            onTrue.remove(i);
            onFalse.remove(j);
            if (j == 0 || i == 0) break;
            i--;
            j--;
        }

        return shared;
    }
}
