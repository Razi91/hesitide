package org.jkonieczny.hesit.language;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jkonieczny on 29.12.14.
 */
public class Function {
    String name;
    String args[];
    ArrayList<Code> codes = new ArrayList<>();

    public Function(String name, String args[]) {
        this.name = name;
        this.args = args;
    }
    public Function(String name) {
        this.name = name;
        this.args = null;
    }

    public String[] getArgs() {
        return args;
    }

    public String getName() {
        return name;
    }

    public void setCodes(List<Code> codes) {
        this.codes = new ArrayList<>();
        this.codes.addAll(codes);
    }

    public void addCodes(Code... codes) {
        this.codes.addAll(Arrays.asList(codes));
    }

    public void addCodes(ArrayList<Code> codes) {
        this.codes.addAll(codes);
    }

    public ArrayList<Code> getCodes(){
        return codes;
    }
}
