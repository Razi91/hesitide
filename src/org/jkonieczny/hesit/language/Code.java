package org.jkonieczny.hesit.language;

import org.jkonieczny.hesit.language.instructions.Instruction;

import java.util.ArrayList;

/**
 * Created by jkonieczny on 29.12.14.
 */
public class Code {
    long id;

    public Code(long id) {
        this.id = id;
    }

    public long getId(){
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Code && this.id == ((Code) obj).id;
    }
}
