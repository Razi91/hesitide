package org.jkonieczny.hesit.language;

import org.jkonieczny.hesit.language.instructions.Condition;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jkonieczny on 29.12.14.
 */
public class Loop extends Code {

    ArrayList<Code> codes;
    Condition condition;

    public Condition getCondition() {
        return condition;
    }


    public Loop(long id, Condition condition, ArrayList<Code> codes) {
        super(id);
        this.codes = codes;
        this.condition = condition;
    }

    public Loop(long id, Condition condition) {
        super(id);
        this.codes = new ArrayList<>();
    }

    public void setOn(boolean val, ArrayList<Code> codes) {
        if (val)
            this.codes = codes;
    }

    public boolean endsAt(Code block) {
        if (codes.size() > 0 && codes.get(codes.size() - 1).equals(block))
            return true;
        boolean ot = codes.stream().filter(c -> c instanceof Loop).anyMatch(c -> ((Loop) c).endsAt(this));
        return ot;
    }

    public void add(ArrayList<Code> codes) {
        this.codes.addAll(codes);
    }

    public void add(Code... codes) {
        this.codes.addAll(Arrays.asList(codes));
    }

    public ArrayList<Code> getCodes() {
        return codes;
    }
    
}
